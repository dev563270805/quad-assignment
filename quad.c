#include <stdio.h>

int main() {
    int a, b, c;
    printf("Enter the first integer: ");
    scanf("%d", &a);
    printf("Enter the second integer: ");
    scanf("%d", &b);
    printf("Enter the third integer: ");
    scanf("%d", &c);

    int discriminant = b * b - 4 * a * c;

    if (discriminant >= 0) {
        double root1 = (-b + sqrt(discriminant )) / (2 * a);
        double root2 = (-b - sqrt(discriminant)) / (2 * a);

        if (root1 == root2) {
            printf("Root: %f\n", root1);
        } else {
            printf("Root 1: %f\n", root1);
            printf("Root 2: %f\n", root2);
        }
    } else {
        printf("Complex roots\n");
    }

    return 0;
}
